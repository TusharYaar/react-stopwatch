import React, { Component } from "react";

import "./TopBar.css";

import clock from "../../clock.png";

export default class TopBar extends Component {
  render() {
    return (
      <div className="top-bar">
        React StopWatch <img src={clock} alt="C" className="clock-icon" />
      </div>
    );
  }
}
