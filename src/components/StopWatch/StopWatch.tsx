import React, { Component } from "react";

import "./StopWatch.css";

type State = {
  stopwatchRunning: boolean;
  time: number;
};

export default class StopWatch extends Component {
  state: State = {
    stopwatchRunning: false,
    time: 0,
  };

  private interval: any;

  formatTime() {
    const { time } = this.state;
    const minute = Math.floor(time / 6000);
    const second = Math.floor((time % 6000) / 100);
    const mSecond = Math.floor(time % 100);

    return `${minute > 9 ? minute : "0" + minute}:${second > 9 ? second : "0" + second}:${
      mSecond > 9 ? mSecond : "0" + mSecond
    }`;
  }

  startStopwatch() {
    this.interval = setInterval(() => {
      this.setState({ time: this.state.time + 1, stopwatchRunning: true });
    }, 10);
  }

  stopStopwatch() {
    if (this.interval) clearInterval(this.interval);
    this.setState({ stopwatchRunning: false });
  }

  resetStopwatch() {
    if (this.interval) clearInterval(this.interval);
    this.setState({ time: 0, stopwatchRunning: false });
  }

  render() {
    return (
      <div className="stopwatch">
        <div className="time">{this.formatTime()}</div>
        <div className="btn-container">
          {this.state.stopwatchRunning ? (
            <button className="btn" onClick={this.stopStopwatch.bind(this)}>
              Pause
            </button>
          ) : (
            <button className="btn" onClick={this.startStopwatch.bind(this)}>
              Start
            </button>
          )}
          <button className="btn" onClick={this.resetStopwatch.bind(this)}>
            Reset
          </button>
        </div>
      </div>
    );
  }
}
