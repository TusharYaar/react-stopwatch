import React, { Component } from "react";
import BottomBar from "../BottomBar/BottomBar";
import StopWatch from "../StopWatch/StopWatch";
import TopBar from "../TopBar/TopBar";

import "./Window.css";

export default class Window extends Component {
  render() {
    return (
      <div className="window">
        <div className="taskbar">
          <div className="taskbar-btn red"></div>
          <div className="taskbar-btn yellow"></div>
          <div className="taskbar-btn green"></div>
        </div>
        <div className="body">
          <TopBar />
          <StopWatch />
          <BottomBar />
        </div>
      </div>
    );
  }
}
